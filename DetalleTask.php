<?php 
$ID = isset($_REQUEST['Task']) ? $_REQUEST['Task']:'0';

 ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>DETALLE</title>
	<link rel="stylesheet" href="">
		<link rel="stylesheet" href="">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
  <link rel="stylesheet" href="style.css">
  <link rel="stylesheet" href="album.css">

</head>
<body>

<main role="main" >
	
<input type="hidden" name="" id="id_task" value="<?php echo $ID; ?>">
  <section class="jumbotron text-center" style=" background-image: url('3840x2160_paper-dents-texture.jpg');" >
    <div class="container" >
      <h1 class="jumbotron-heading" id="titulo_task"></h1>
      <p class="lead text-muted" id="nota_task"></p>
      <p>
          <a href="" class="btn btn-primary my-2" data-bs-toggle="modal" data-bs-target="#exampleModal">Agregar Archivo</a>
          <a href="#" class="btn btn-secondary my-2">Agregar Nota</a>
      </p>
    </div>
  </section>

</main>
<div class="container">

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Cargar Archivo</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
<form id="form-archivo" action="javascript: enviar(this)" enctype=" multipart/form-data"> 
  <div class="row mb-3">
    <label for="inputEmail3" class="col-sm-3 col-form-label">Titulo</label>
    <div class="col-sm-9">
      <input type="text" class="form-control" id="inputEmail3" name="titulo">
    </div>
  </div>
  <div class="row mb-3">
    <label for="inputPassword3" class="col-sm-3 col-form-label">Descripción </label>
    <div class="col-sm-9">
      <textarea class="form-control" name="descrip"></textarea> 
    </div>
  </div>
  <div class="row mb-3">
  	<label for="input4" class="col-sm-3 col-form-label">Archivo</label>
<div class="col-sm-9">
	
<input type="file" name="archivo" class="form-control" value="" placeholder="">
</div>
  </div>

<div class="progress">
  <div id="progressbar" class="progress-bar progress-bar-striped progress-bar-animated bg-success"  role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>
</div>

</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>


</div>



	
</body>
<footer>
		
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
	<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
	<script type="text/javascript" src="detalle.js"></script>

</footer>
</html>